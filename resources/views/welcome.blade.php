@extends('layout.auth')
@section('content')
<div class="content">
   <div id="myModal" class="modal">
      <div class="modal-content">
         <a href="login.html">
            <!--       <img src="https://brands.live/public/front/assets/img/popup/5premiumimagefree.png" style="width: 100%"> -->
            <img src="{{ asset('img/popup/popup.png') }}" style="width: 100%">
         </a>
      </div>
   </div>
   <input type="hidden" id="ref" > 
   <!-- <section class="" style="padding: 30px 0">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="single-fun-fact wow fadeInRight" data-wow-delay="0.3s">
                      <h3><span class="odometer" data-count="2816059">0</span>+</h3>
                      <p>Business Registered</p>
                  </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="single-fun-fact wow fadeInRight" data-wow-delay="0.6s">
                      <h3><span class="odometer" data-count="12502802">00</span>+</h3>
                      <p>Posts Download</p>
                  </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="single-fun-fact wow fadeInRight" data-wow-delay="0.9s">
                      <h3><span class="odometer" data-count="1309016">00</span>+</h3>
                      <p>Creative Options</p>
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <!--TESTMONIAL AREA-->
   <section class="testmonial-area section-padding fix" id="process" style="position: relative;top: 20px;padding-bottom: 0;" >
      <div class="testmonial-layers">
         <div class="testmonial-layer-1"><img src="{{ asset('assets/img/bg-layers/layers-1.html') }}" alt=""></div>
         <div class="testmonial-layer-2"><img src="{{ asset('assets/img/bg-layers/layers-2.html') }}" alt=""></div>
         <div class="testmonial-layer-3" ><img src="{{ asset('assets/img/bg-layers/layers-3.html') }}" alt=""></div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-lg-12  col-sm-12 col-xs-12">
               <div class="area-title" style="margin-bottom: 0;">
                  <!-- <h4 class="subtitle">Upcoming Events</h4> -->
                  <h2>Upcoming Events</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="testmonial-slider">
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1605864114.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1669375729.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1634201230.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1631094119.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1667447749.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1634201445.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >27 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1667448122.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >27 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1667448457.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >27 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1634201981.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637922091.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >29 Nov 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1606561656.png">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636607928.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636608900.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668491966.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668492261.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636894893.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636615512.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636610284.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636614481.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637307293.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668492561.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668668946.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636620122.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607927540.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636616924.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636619190.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636975398.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637308252.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1638431169.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668492771.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668493246.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668493783.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636803584.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >04 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637240650.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >04 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637296127.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >04 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668666104.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >04 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668666310.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >04 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636895820.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >05 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636699590.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >05 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668666685.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >05 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668667256.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >05 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668667518.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >05 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637919068.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >06 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1638769688.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >06 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668669260.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >06 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636897219.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637313510.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637128859.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637297704.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1638338468.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668669717.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637129581.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668669898.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668670211.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668670453.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668670751.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668681006.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668681262.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636969887.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >09 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637903442.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >09 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668681823.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >09 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607336857.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >10 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636972394.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >10 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636973200.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >10 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637299232.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >10 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1629884004.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >11 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637305979.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >11 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637310270.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >11 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637929899.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >11 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1668683441.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >11 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639290472.png">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >12 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639301630.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >12 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1669033107.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >12 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636974873.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >13 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637930542.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >13 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637921870.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >14 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636969044.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >14 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637309171.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >14 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607434038.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >15 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607685333.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >15 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637390000.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >16 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637126647.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >18 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637126992.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >18 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637127712.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >18 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1632985348.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637128415.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637310763.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639733737.png">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639738270.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639738302.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >19 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637130634.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >20 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1639718599.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >20 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637386680.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >21 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607685242.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >22 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1644916857.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >22 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607594905.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >23 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637388582.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >23 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607596011.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >24 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607772001.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607687806.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1626677688.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1634135748.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636972570.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1640414904.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1640415222.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1608719474.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1636805111.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637391006.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637406215.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >27 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1626770965.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637407987.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637408610.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1640674536.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1609479184.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >29 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1640603269.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >29 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1607927075.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >30 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637408980.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >30 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1637409553.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >30 Dec 2022</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1609482844.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >12 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1641794512.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >13 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1641895096.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >16 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1609831648.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >18 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1642484705.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1611896407.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >26 Jan 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1646644695.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >12 Feb 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1612419322.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >13 Feb 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1613727632.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 Mar 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1649486636.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >09 Mar 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1616392786.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >22 Mar 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1616501181.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >30 Mar 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1616657423.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >06 Apr 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1615379970.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >07 Apr 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1616231894.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >14 Apr 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1649056247.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >14 Apr 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1617082820.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >21 Apr 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1650861822.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >03 May 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1618576565.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >08 May 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1621825088.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Jun 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1622120014.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >09 Jun 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1654499967.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >17 Jun 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1624602550.png">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >25 Jun 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1661753675.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >28 Sep 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1631535967.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Oct 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1663911124.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >02 Oct 2023</p>
                     </div>
                  </div>
                  <div class="single-testmonial">
                     <div class="author-content">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://d3jbu7vaxvlagf.cloudfront.net/public/small/flyer_image/festival_image/Fest_1634381029.jpg">
                        <!-- <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg"> -->
                        <p >01 Nov 2023</p>
                     </div>
                  </div>
                  <!-- <div class="single-testmonial">
                     <div class="author-content">
                         <img src="https://brands.live/public/front/assets/img/download_images/anant_chaturdashi/1.jpg">
                         <p >24 Dec 2022</p>
                     </div>
                     </div> -->
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--TESTMONIAL AREA END-->
   <!--TEAM AREA-->
   <!-- <section class="team-area padding-top-50 gray-bg" >
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                  <div class="area-title" style="margin-bottom: 0;">
                      <h2>Business Categories</h2>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="business-categories">
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/christmas.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/Makarsankranti.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/lohri.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/pongal.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/holi.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/raksha-bandhan.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/Janmasthmi.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/akshaya-tritiya.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/diwali.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/dhanteras.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      <div class="single-team">
                          <div class="member-thumb">
                              <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/bhaidooj.jpg" alt="">
                          </div>
                          <p>Real Estate</p>
                      </div>
                      
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <!--TEAM AREA END-->
   <!--TEAM AREA-->
   <section class="team-area padding-top-50 " >
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
               <div class="area-title" style="margin-bottom: 0;">
                  <!-- <h4 class="subtitle">India Festivals</h4> -->
                  <h2>Festivals Of India</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="team-slider-two">
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/christmas.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/Makarsankranti.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/lohri.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/pongal.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/holi.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/raksha-bandhan.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/Janmasthmi.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/akshaya-tritiya.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/diwali.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/dhanteras.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/indiafestivals/bhaidooj.jpg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--TEAM AREA END-->
   <!--TEAM AREA-->
   <section class="team-area padding-top-50 gray-bg">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
               <div class="area-title" style="margin-bottom: 0;">
                  <!-- <h4 class="subtitle">Daily Categories</h4> -->
                  <h2>Daily Categories</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="team-slider-two">
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/good_morning/11.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img//download_images/good_thoughts/1.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/devotional/4.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/motivation/4.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/leaders_quote/8.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/brandspot_special/2.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/aatmanirbhar_bharat/1.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/sports/8.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/good_night/8.jpg" alt="">
                     </div>
                  </div>
                  <!-- <div class="single-team">
                     <div class="member-thumb">
                         <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/img/download_images/panchangam/2.jpg" alt="">
                     </div>
                     </div> -->
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/bhagvat_geeta_shlok/9.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/memes/1.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/trending/9.jpg" alt="">
                     </div>
                  </div>
                  <div class="single-team">
                     <div class="member-thumb">
                        <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/download_images/business_news/9.jpg" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--TEAM AREA END-->
   <!-- <section class="process-area padding-top section-padding gray-bg" id="works">
      <div class="container">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                  <div class="area-title center">
                      <h2>How Does Brands.live Works</h2>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="process-content">
                      <div class="area-bg"></div>
                      <div class="row">
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="single-process-box text-icon-box xs-mb50 wow fadeInLeft" data-wow-delay="0.6s">
                                  <div class="process-icon"><i class="dripicons-upload"></i>
                                  </div>
                                  <h3>Login/Signup</h3>
                                  <p>Login to Brands.live portal with your mobile number</p>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="single-process-box text-icon-box hidden-sm wow fadeInLeft" data-wow-delay="0.9s">
                                  <div class="process-icon"><i class="fa fa-picture-o"></i>
                                  </div>
                                  <h3>Select Creatives</h3>
                                  <p>We have wide range of categories to choose best marketing post</p>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="single-process-box text-icon-box hidden-sm wow fadeInLeft" data-wow-delay="0.9s">
                                  <div class="process-icon"><i class="fa fa-share"></i>
                                  </div>
                                  <h3>Download & Share</h3>
                                  <p>Download and share it on all your social media/digital platforms</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <section class="features-area section-padding fix " id="features">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
               <div class="area-title sm-center xs-center" style="margin-bottom:0">
                  <h2 class="mb0">Set of Features</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="">
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-briefcase" aria-hidden="true"></i>
                     </div>
                     <h3>Business Posts</h3>
                     <p> You will get 200+ Marketing Post for your Business Categories</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                     </div>
                     <h3>Festival Posts</h3>
                     <p>Get personalized festival posts to be connected with your clients</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-plus" aria-hidden="true"></i>
                     </div>
                     <h3>Custom Templates</h3>
                     <p>Ready to use templates which fulfil all your marketing needs</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-play" aria-hidden="true"></i>
                     </div>
                     <h3>Image to Video</h3>
                     <p>Convert your images into creative videos with our inbuilt effects</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-globe" aria-hidden="true"></i>
                     </div>
                     <h3> One Page Website</h3>
                     <p>Get your business website to stand out from your competitors</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-id-card" aria-hidden="true"></i>
                     </div>
                     <h3>Digital Business Card</h3>
                     <p>Get creative business card to grab attention of your clients</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-clock-o" aria-hidden="true"></i>
                     </div>
                     <h3>Schedule Post</h3>
                     <p>Yes, you can download and schedule post for Facebook and Instagram</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="text-icon-box single-features">
                     <div class="box-icon"><i class="fa fa-object-ungroup" aria-hidden="true"></i>
                     </div>
                     <h3>Logo Maker</h3>
                     <p>Create stunning logo which suits your business</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--  <section class="features-area section-padding fix">
      <div class="container">
          <div class="row">
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <div class="area-title sm-center xs-center">
                      <h4 class="subtitle">Categories</h4>
                      <h2>Categories of Images and Videos To Explore</h2>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="">
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/BusinessMarketing.jpg">
                          <h3>Business Marketing </h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/festival.jpg">
                          <h3>Festival Greetings</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/International.jpg">
                          <h3>International Day</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/BusinessEthics.jpg">
                          <h3>Business Ethics</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/SportsQuotes.jpg">
                          <h3>Sports Quotes</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/Motivational.jpg">
                          <h3>Motivational Quotes</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/LeadersQuotes.jpg">
                          <h3>Leaders’ Quotes</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/DevotionalQuotes.jpg">
                          <h3>Devotional Quotes</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/CoronaTips.jpg">
                          <h3>Corona Tips</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/AatmaNirbharBharat.jpg">
                          <h3>AatmaNirbhar Bharat</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/GoodMorning.jpg">
                          <h3>Good Morning and Good Night</h3>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6">
                      <div class="text-icon-box single-features">
                          <img src="https://brands.live/public/front/assets/img/businesscat/BusinessRequirement.jpg">
                          <h3>Business Requirement Posts</h3>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <!-- <section class="about-area section-padding gray-bg" id="services">
      <div class="container">
          <div class="row">
              <div class="col-md-5 col-lg-5 col-sm-6 col-xs-12">
                  <div class="area-image-content sm-mb0 xs-mb10 wow fadeInRight" data-wow-delay="0.3s">
                      <img src="https://brands.live/public/front/assets/img/about/about-mockup-new.png" alt="">
                  </div>
              </div>
              <div class="col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
                  <div class="area-content">
                      <h3 class="wow fadeInRight" data-wow-delay="0.3s">TRANSFORM YOUR BUSINESS INTO A BRAND</h3>
                      <div class="text-icon-box details-content-box wow fadeInRight" data-wow-delay="0.6s">
                          <div class="box-icon"><i class="dripicons-graph-pie"></i>
                          </div>
                          <p>Get branding images for your business business which helps you to stay connected with your clients through out a year. You can download daily one images from app and share it on social media platforms like facebook, twitter, instagram, linkedin, whatsapp etc...</p> <a href="https://onelink.to/wqp5fz" class="read-more active">Download Now</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <section class="section-padding gray-bg">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="area-content xs-center xs-mb50">
                  <h4 class="subtitle">Customize any template, or design something more personal, like an invitation.</h4>
                  <h3>Templates for absolutely anything</h3>
               </div>
            </div>
         </div>
         <div class="row" data-masonry="{&quot;percentPosition&quot;: true }">
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/1.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/2.png" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/3.png" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/4.png" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/5.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/6.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/7.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/8.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/10.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/11.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/12.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/13.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/14.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/9.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/15.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/16.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/17.jpg" style="padding:5px;border-radius: 7px;">
            </div>
            <div class="col-sm-3 col-lg-2 p-0">
               <img class="lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="https://brands.live/public/front/assets/img/templates/18.jpg" style="padding:5px;border-radius: 7px;">
            </div>
         </div>
      </div>
   </section>
   <section class="video-promo-area section-padding white" id="video">
      <div class="area-bg"></div>
      <div class="container">
         <div class="row flex-v-center">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
               <div class="area-image-content sm-center xs-center sm-mb50 wow fadeInRight" data-wow-delay="0.3s">
                  <div class="video-bg-layer" >
                     <img src="{{ asset('img/video/dots-layer.png') }}" alt="">
                  </div>
                  <img src="{{ asset('img/video/video-bg-new5.jpg') }}" alt="">
                  <div class="video-button item-center">
                     <a href="#myModalVideo" data-toggle="modal" class="" target="_blank"><i class="fa fa-play"></i></a>
                     <!-- <button class="video-popup" data-video-id="ThtT8Mkts4w">
                        </button> -->
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
         </div>
      </div>
   </section>
   <!-- <section class="screenshot-area fix section-padding" id="screenshots">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-sm-5 col-xs-12">
                  <div class="area-content xs-center xs-mb50">
                      <h4 class="subtitle">Clean Design</h4>
                      <h3>Brands.live Apps Screenshot</h3>
                  </div>
              </div>
              <div class="col-md-8 col-sm-7 col-xs-12">
                  <div class="screenshot-slider">
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/7.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/1.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/2.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/3.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/4.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/5.png" alt="">
                      </div>
                      <div class="single-screenshot">
                          <img src="https://brands.live/public/front/assets/img/screenshots/new/6.png" alt="">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </section> -->
   <section class="contact-area padding-100-50 gray-bg" id="contact">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
               <div class="area-title center">
                  <!-- <h4 class="subtitle">Contact Us</h4> -->
                  <h2>Contact Us</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
               <div class="contact-content padding50 mb50 row justify-content-center">
                  <div class="col-md-4">
                     <div class="single-contact">
                        <div class="contact-icon"><i class="dripicons-location"></i>
                        </div>
                        <h3>Location</h3>
                        <p>CredApp Software Pvt. Ltd., 44/45, Gate no. 5, Shree Krishnanagar Society Opp. Sarvamangal Hall, Memnagar, Ahmedabad, Gujarat 380052
                        </p>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="single-contact">
                        <div class="contact-icon"><i class="dripicons-mail"></i>
                        </div>
                        <h3>Email</h3>
                        <p style="white-space: nowrap"><a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="e5868a8b91848691a58797848b8196cb898c9380">[email&#160;protected]</a></p>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="single-contact">
                        <div class="contact-icon"><i class="dripicons-phone"></i>
                        </div>
                        <h3>Phone</h3>
                        <p>+91-7069365365</p>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <div class="col-md-5 col-lg-5 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
               <div class="contact-form mb50">
                   <form action="http://quomodosoft.com/html/applic/applic/process.php" id="contact-form" method="post">
                       <div class="row">
                           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                               <div class="form-group" id="name-field">
                                   <div class="form-input">
                                       <label for="form-name">Your Name</label>
                                       <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Add Your Name" required>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                               <div class="form-group" id="email-field">
                                   <div class="form-input">
                                       <label for="form-email">Your Mail</label>
                                       <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Add Your Email" required>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                               <div class="form-group" id="phone-field">
                                   <div class="form-input">
                                       <label for="form-phone">Your Subject</label>
                                       <input type="text" class="form-control" id="form-phone" name="form-phone" placeholder="Subject..">
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                               <div class="form-group" id="message-field">
                                   <div class="form-input">
                                       <label for="form-message">Your Message</label>
                                       <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Your Message" required></textarea>
                                   </div>
                               </div>
                           </div>
                           <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                               <div class="form-group mb0">
                                   <button class="read-more active" type="submit">Send Message</button>
                               </div>
                           </div>
                       </div>
                   </form>
               </div>
               </div> -->
         </div>
      </div>
   </section>
   <footer class="footer-area white relative">
      <div class="area-bg"></div>
      <div class="footer-top-area section-padding-footer">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 sm-center xs-center sm-mb50 xs-mb0 text-center">
                  <div class="single-widgets">
                     <div class="footer-logo">
                        <a href="#">
                        <img src="{{ asset('img/brandslive_white.png') }}" alt="">
                        </a>
                     </div>
                     <p class="social-icons"><a href="https://www.facebook.com/Brands.liveofficial/" class="text-white"><i style="padding-top: 7px;" class="fa fa-facebook"></i></a><a href="https://twitter.com/brandsdotlive" class="text-white"><i style="padding-top: 7px;" class="fa fa-twitter"></i></a><a href="https://www.instagram.com/Brands.liveofficial" class="text-white"><i style="padding-top: 7px;" class="fa fa-instagram"></i></a><a href="https://www.youtube.com/brandsdotlive/" class="text-white"><i class="fa fa-youtube" style="padding-top: 7px;"></i></a><a href="https://in.pinterest.com/brandsdotlive" class="text-white"><i class="fa fa-pinterest" style="padding-top: 7px;"></i></a></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom-area">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-lg-6 col-sm-12 col-xs-12">
                  <div class="footer-copyright">
                     <p>Copyright &copy; <a href="#"> Brands.live</a> All Right Reserved.</p>
                  </div>
               </div>
               <div class="col-lg-6">
                  <p class="pull-right footer-copyright">
                     <a href="privacy_policy.html">Privacy Policy</a>
                     |
                     <a href="terms_condition.html">Terms & Condition</a>
                     |
                     <a href="refund_cancellation_policy.html">Refund policy</a>
                     |
                     <a href="faq.html">Faqs</a>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </footer>
</div>
<div id="myModalVideo" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- <div class="modal-header">
            <h5 class="modal-title">YouTube Video</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>                
            </div> -->
         <div class="modal-body">
            <div class="embed-responsive embed-responsive-16by9">
               <iframe id="cartoonVideo" class="embed-responsive-item" height="600"  src="https://www.youtube.com/embed/YBynMFDcwl4?autoplay=1" allowfullscreen autoplay></iframe>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection